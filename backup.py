#!/usr/bin/env python

import os
import shutil
from optparse import OptionParser

import common

def backup_file(path, backup_path):
    # Try to skip copying if possible
    if os.path.isfile(backup_path):
        if common.files_are_same(path, backup_path):
            return

    shutil.copyfile(path, backup_path)

def backup_folder(path, backup_path):
    if not os.path.isdir(backup_path):
        os.makedirs(backup_path)
    file_count = common.folder_file_count(path)
    for i in range(1, file_count+1):
        filename = common.nth_file(i)
        backup_file(path + '/' + filename, backup_path + '/' + filename)

def backup_dataset(path, backup_path):
    for subfolder in os.listdir(path):
        backup_folder(path + '/' + subfolder, backup_path + '/' + subfolder)

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()
    if len(args) != 2:
        print 'Usage: ./backup.py <location> <backup_location>'

    path = args[0]
    backup_path = args[1]

    backup_dataset(path, backup_path)

if __name__ == '__main__':
    main()
