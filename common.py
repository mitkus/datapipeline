import os
import string
import random

max_line = 80
alphabet = string.ascii_letters
fileprefix = 'file'

# Returns true if files have same size
def files_are_same(filepath1, filepath2):
    # This is not very complete check, might be worth comparing modification
    # timestamps and/or content checksums for larger datasets.
    size1 = os.path.getsize(filepath1)
    size2 = os.path.getsize(filepath2)
    return size1 == size2

# Returns file size for all but last file in a dataset subfolder
def folder_file_size(path):
    return os.path.getsize(path + '/' + nth_file(1))

def folder_file_count(path):
    cnt = 0
    for filename in os.listdir(path):
        if filename.startswith(fileprefix):
            cnt += 1
    return cnt

# Returns name for nth data file
def nth_file(n):
    return fileprefix + str(n)

# Generates random text string between 1 and max_line.
# If remaining_bytes can be filled with a single line,
# returns appropriate length line.
def random_line(remaining_bytes):
    line_length = remaining_bytes-1
    if line_length > max_line:
        line_length = random.randrange(1, max_line)
    return ''.join([random.choice(alphabet) for _ in range(line_length)])

# Parses dataset layouts like "locations,64,sensors,32" into a dictionary
def parse_layout(layout_str):
    layout = {}
    layout_list = layout_str.split(',')
    for i in range(len(layout_list)/2):
        layout[layout_list[i*2]] = int(layout_list[i*2+1]) 
    return layout

