#!/usr/bin/env python

import os
from optparse import OptionParser

import common

def generate_file(path, size):
    size_in_bytes = size * 1024 * 1024
    f = open(path, 'w')
    current_size = 0
    while current_size < size_in_bytes:
        remaining = size_in_bytes - current_size
        line = common.random_line(remaining) + '\n'
        f.write(line)
        current_size += len(line)
    f.close()
    return size

def generate_folder(path, folder_size, file_size):
    os.makedirs(path)
    current_size, current_file = 0, 1
    while current_size < folder_size:
        megabytes_remaining = folder_size - current_size
        current_size += generate_file(path + '/' + common.nth_file(current_file),
                min(file_size, megabytes_remaining))
        current_file += 1

def generate_dataset(path, file_size, layout):
    for subfolder, folder_size in layout.iteritems():
        generate_folder(path + '/' + subfolder, folder_size, file_size)
        
def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()
    if len(args) != 3:
        print "Usage: ./generate.py <location> <size> <name1>,<size1>,<name2>,<size2>" 
        return
    
    path = args[0]
    file_size = int(args[1])
    layout = common.parse_layout(args[2])

    generate_dataset(path, file_size, layout)


if __name__ == '__main__':
    main()
