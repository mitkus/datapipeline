#!/usr/bin/env python

from optparse import OptionParser

import common

def update_file(path, delta_size):
    if delta_size == 0:
        return

    f = open(path, 'a')
    written = 0
    while written < delta_size:
        remaining = delta_size - written 
        line = common.random_line(remaining) + '\n'
        f.write(line)
        written += len(line) 
    f.close()

def update_folder(path, size):
    file_size = common.folder_file_size(path)
    file_count = common.folder_file_count(path)

    size_in_bytes = size * 1024 * 1024
    delta_size = size_in_bytes // file_count
    last_file_delta_size = size_in_bytes - (delta_size * (file_count-1))

    for i in range(1, file_count):
        update_file(path + '/' + common.nth_file(i), delta_size)

    # Last file might be updated by different size
    update_file(path + '/' + common.nth_file(file_count),
            last_file_delta_size)

def update_dataset(path, layout):
    for subfolder, folder_size in layout.iteritems():
        update_folder(path + '/' + subfolder, folder_size)

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()
    if len(args) != 2:
        print 'Usage: ./update.py <location> <name1>,<size1>,<name2>,<size2>'

    path = args[0]
    layout = common.parse_layout(args[1])

    update_dataset(path, layout)

if __name__ == '__main__':
    main()
